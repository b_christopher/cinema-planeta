document.addEventListener("DOMContentLoaded", function(event){
    
    var body = document.body,
    overlay = document.querySelector('.signup-overlay'),
    wrap = document.querySelector('.signup-wrap'),
    overlayBtts = document.querySelectorAll('.open-btn');
        
    [].forEach.call(overlayBtts, function(btt) {

      btt.addEventListener('click', function() { 
          
          console.log();
         
         /* Detect the button class name */
         var overlayOpen = this.className === 'open-btn';
         
         /* Toggle the aria-hidden state on the overlay and the 
            no-scroll class on the body */
         //overlay.setAttribute('aria-hidden', overlayOpen);
         jQuery(overlay).toggle();
         jQuery(wrap).toggle(350);
          
         body.classList.toggle('noscroll', !overlayOpen);
         
         /* On some mobile browser when the overlay was previously
            opened and scrolled, if you open it again it doesn't 
            reset its scrollTop property */
         overlay.scrollTop = 0;

      }, false);

    });    
    
    //Close Button for Signup Form
    jQuery('.signup-overlay-x').click(function(){
        console.log('clicked');
        jQuery(overlay).toggle();
        jQuery(wrap).toggle(350);
        body.classList.toggle('noscroll');
        overlay.scrollTop = 0;

    });
    
    //Validate Register Form
    jQuery(".register-form").validate({
        rules: {
            registerpass: {
                minlength: 5
            },
            registerpassconfirm: {
                minlength: 5,
                equalTo: "#registerpass"
            }
        },
        success: function(){
                console.log('valid!');
        }
    });
    
//Open Registration Plan Options
jQuery('.signup-submit').click(function(e){
    
    $valid = jQuery('.register-form').valid();
    
    if($valid == true){
        e.preventDefault();
        jQuery('.options-overlay').show();
    }
    
});

jQuery('.options-overlay-x').click(function(){
   jQuery('.options-overlay').hide(); 
});
    
jQuery('.success-link').click(function(){
   jQuery('.options-overlay').hide(); 
});
    
//jQuery('.full-btn').click(function(){
//   jQuery('.plan-row').hide(); 
//   jQuery('.paypal-setup-row').show(); 
//});
    
//    jQuery('.register-form input').on('blur', function() {
//        if (jQuery(".register-form").valid()) {
//            jQuery('.signup-submit').prop('disabled', false);  
//        } else {
//            jQuery('.signup-submit').prop('disabled', 'disabled');
//        }
//        
//    });
    
    //Menu Dropdown
    jQuery('.menu-toggle-btn').click(function(){
        jQuery('.main-navigation').toggle(500); 
    });
    
    //Player popup on Main Page
    jQuery('.main-page .play-movie-btn').click(function(){
       jQuery('.player-wrap').css('display', 'flex'); 
       jQuery('body').css('overflow', 'hidden'); 
    });
    
    jQuery('.player-wrap').click(function(){
       jQuery('.player-wrap').css('display', 'none'); 
       jQuery('body').css('overflow', 'visible'); 
    });
    
    //Player Popup on single page
    jQuery('.single-play-btn').click(function(){
        jQuery('.player-wrap').css('display', 'flex'); 
        jQuery('body').css('overflow', 'hidden'); 
    });
    
    //Player Popup for Podcast
    jQuery('.podcast-thumb').click(function(){
        jQuery(this).prev().css('display', 'flex'); 
        jQuery('body').css('overflow', 'hidden'); 
    });
    
    jQuery('.podcast-player-wrap').click(function(){
       jQuery(this).css('display', 'none'); 
       jQuery('body').css('overflow', 'visible'); 
    });
    
    
    //Add placeholders to login form
    jQuery('#user').attr( 'placeholder', 'Email' );
    jQuery('#pass').attr( 'placeholder', 'Password' );
    
    //Make Signin Visible on url
    var url = window.location.href;
    // Get DIV
    var overlay = document.getElementById('overlay');
    var wrap = document.getElementById('wrap');
    // Check if URL contains the keyword
    if( url.search( 'signin' ) > 0 ) {
        // Display the message
        jQuery(overlay).toggle();
        jQuery(wrap).toggle();
        body.classList.toggle('noscroll');
        overlay.scrollTop = 0;
    }
    
    if( url.search( 'paypal' ) > 0 ) {
    
        //Display PayPal Popup
        jQuery('.options-overlay').show();
        jQuery('.plan-row').hide(); 
        jQuery('.paypal-success-row').show(); 
    }
    
});

//AJAX For Registration
jQuery(document).on( 'click', '.pay-btn', function(e) {
    e.preventDefault();
    
    $username = jQuery('#register-user').val();
    $firstname = jQuery('#register-first').val();
    $lastname = jQuery('#register-last').val();
    $email = jQuery('#register-email').val();
    $pass = jQuery('#registerpass').val();
    
	jQuery.ajax({
		url: cinemajs.ajax_url,
		type: 'post',
		data: {
			action: 'cp_register_user',
            username: $username,
            firstname: $firstname,
            lastname: $lastname,
            email: $email,
            pass: $pass
        },
        success: function( response ) {
			jQuery('.plan-row').hide();
            jQuery('.success-row').show();
		}
	});
});

jQuery(document).on( 'click', '.sub-btn', function(e) {
    
    $username = jQuery('#register-user').val();
    $firstname = jQuery('#register-first').val();
    $lastname = jQuery('#register-last').val();
    $email = jQuery('#register-email').val();
    $pass = jQuery('#registerpass').val();
    
	jQuery.ajax({
		url: cinemajs.ajax_url,
		type: 'post',
		data: {
			action: 'cp_register_user',
            username: $username,
            firstname: $firstname,
            lastname: $lastname,
            email: $email,
            pass: $pass
        },
        success: function( response ) {
            jQuery('.plan-row').hide();
            jQuery('.paypal-setup-row').show();
		}
	});
});