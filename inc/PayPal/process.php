<?php 

//Here we can use PayPal URL or sandbox URL.
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//Here we can used seller email id.
$merchant_email = 'merchants email id';
//here we can put cancel URL when payment is not completed.
$cancel_return = "https://www.formget.com/tutorial/paypal-subscription/index.php";
//here we can put cancel URL when payment is Successful.
$success_return = "https://www.formget.com/tutorial/paypal-subscription/success.php";
?>
<div style="margin-left: 38%"><img src="images/ajax-loader.gif"/><img src="images/processing_animation.gif"/></div>
<form name = "myform" action = "<?php echo $paypal_url; ?>" method = "post" target = "_top">
<input type="hidden" name="cmd" value="_xclick-subscriptions">
<input type = "hidden" name = "business" value = "<?php echo $merchant_email; ?>">
<input type="hidden" name="lc" value="IN">
<input type = "hidden" name = "item_name" value = "<?php echo $product_name; ?>">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="src" value="1">
<?php if (!empty($total_cycle)) { ?>
<input type="hidden" name="srt" value="<?php echo $total_cycle; ?>">
<?php } ?>
<input type="hidden" name="a3" value="<?php echo $cycle_amount; ?>">
<input type="hidden" name="p3" value="1">
<input type="hidden" name="t3" value="<?php echo $cycle; ?>">
<input type="hidden" name="currency_code" value="<?php echo $product_currency; ?>">
<input type = "hidden" name = "cancel_return" value = "<?php echo $cancel_return ?>">
<input type = "hidden" name = "return" value = "<?php echo $success_return; ?>">
<input type="hidden" name="bn" value="PP-SubscriptionsBF:btn_subscribeCC_LG.gif:NonHostedGuest">
</form>