<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Cinema_Planeta
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function cinema_planeta_theme_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'cinema_planeta_theme_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function cinema_planeta_theme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'cinema_planeta_theme_pingback_header' );

// Remove admin bar for users
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }
}

//Change login page
function redirect_login_page() {
	$login_page  = home_url( '/#signin' );
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','redirect_login_page');

//Redirect on logout
function auto_redirect_after_logout(){
  wp_redirect( 'http://cinemaplaneta.tv/#signin' );
  exit();
}
add_action('wp_logout','auto_redirect_after_logout');


add_action('wp_logout','auto_redirect_external_after_logout');
function auto_redirect_external_after_logout(){
  wp_redirect( 'https://trickspanda.com' );
  exit();
}

//Redirect on login fail
function login_failed() {
	$login_page  = home_url( '/' );
	wp_redirect( $login_page . '?login=failed#signin' );
	exit;
}
add_action( 'wp_login_failed', 'login_failed' );

//Redirect on login empty
function verify_username_password( $user, $username, $password ) {
	$login_page  = home_url( '/' );
    if( $username == "" || $password == "" ) {
        
        $referrer = $_SERVER['HTTP_REFERER'];
        
        if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')     ) {
            wp_redirect( $login_page . "?login=empty#signin" );
            exit;
         }

    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);

//Add Footer Menu
function cp_custom_menus() {
  register_nav_menus(
    array(
    'main-menu' => __('Main Menu'),
      'footer-menu-1' => __( 'Footer Menu 1' ),
      'footer-menu-2' => __( 'Footer Menu 2' )
    )
  );
}
add_action( 'init', 'cp_custom_menus' );

function app_output_buffer() {
	ob_start();
} 
add_action('init', 'app_output_buffer');

//User Registration
add_action( 'wp_ajax_nopriv_cp_register_user', 'cp_register_user' );
add_action( 'wp_ajax_cp_register_user', 'cp_register_user' );

function cp_register_user() {
    
    //Form Variables
    $user_name = $_POST['username'];
    $first_name = $_POST['firstname'];
    $last_name = $_POST['lastname'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
	
    //Create new user
    wp_create_user($user_name, $pass, $email);
    
    //Login User
    if ( !is_user_logged_in() ) {
        $user = get_userdatabylogin( $user_name );
        $user_id = $user->ID;
        wp_set_current_user( $user_id, $user_login );
        wp_set_auth_cookie( $user_id );
        do_action( 'wp_login', $user_login );
    }    
    
    echo 'success';
    
    
    die();
}