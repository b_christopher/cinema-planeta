<?php
/* Template Name: Profile Page */ 
 
get_header(); ?>
<div class="profile-page-wrap two-col-sidebar-left">

    <div class="profile-sidebar">
    <h2>My Profile</h2>
    <div class="profile-buttons">
        <button class="change-info active purple-btn">Change Information</button>    
        <button class="payments purple-btn">Payments</button>    
        <button class="plans purple-btn">Plans</button>    
        <button class="noti purple-btn">Notifications (2)</button>    
        <button class="delete purple-btn">Delete Account</button>    
        
    </div>
    </div>
    
    <div class="profile-main">
        <div class="info-wrap">
            
            <div class="profile-img-wrap">
                <img src="/wp-content/themes/cinema-planeta/images/profile-img.png" alt="Profile Image" />
                <button class="change-img purple-btn">Change</button>
            </div>
            
            <form class="update-info">
            
            <div class="two-col-wrap">    
                
                <div class="input-grp">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="username">
                </div>


                <div class="input-grp">    
                    <label for="email" class="email">Email</label>
                    <input type="email" name="email" class="email">
                </div>
                
            </div>
            
            <div class="two-col-wrap">     
                <div class="input-grp">
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" class="firstname">
                </div>

                <div class="input-grp">
                    <label for="password" class="password">Password</label>
                    <input type="password" name="password" class="password">
                </div>
            </div>
                
            <div class="two-col-wrap">     
                <div class="input-grp">            
                    <label for="lastname" class="lastname">Last Name</label>
                    <input type="text" name="lastname" class="lastname">
                </div>

                <div class="input-grp">
                    <label for="password-re" class="password-re">Password (repeat)</label>
                    <input type="password" name="password-re" class="password-re">
                </div>
            </div>
                
            <input type="submit" class="submit-btn blue-btn" name="submit" value="Save Changes">
                
            </form>
            
            <div class="social-link">
                <p class="social-link-txt">Currently your profile is linked with Facebook</p>
                <button class="sign-out-fb purple-btn"><img src="/wp-content/themes/cinema-planeta/images/FBIcon.png" alt="facebook">Sign Out</button>
                <button class="sign-in-google purple-btn"><img src="/wp-content/themes/cinema-planeta/images/GoogleIcon.png" alt="google">Sign In</button>
            </div>
            
        </div>
    </div>
    
    
</div>
<?php 

get_footer();

?>