<?php
/* Template Name: Podcast Page */ 

//Set API Key
\VHX\Api::setKey("GsqqcWX7gEAojnP1ay79HT2DRjLtwR8D");

//List all Videos
$videos = \VHX\Videos::all()['_embedded']['videos'];

$products = \VHX\Products::all();

$collections = \VHX\Collections::all();

$collect_items = $collections['_embedded']['collections'];

//echo '<pre>';
//print_r($collect_items);
//echo '</pre>';

//echo $products['_embedded']['products'][0]['movies'];

get_header(); ?>
<div class="player-wrap">
<?php echo $f_iframe; ?>
</div>

<?php 
    
//Podcasts    
$p_collections = \VHX\Collections::items(39782);    
$podcasts = $p_collections['_embedded']['items'];
//echo '<pre>';    
//print_r($podcasts);    
//echo '</pre>';
?>
  <div class="movie-gallery">  
    <div class="podcasts">
    <h2>Podcasts</h2>
    <div class="recent-grid movie-grid">

    <?php
        
    foreach ($podcasts as $podcast){
        $p_id = $podcast['id'];    
        $p_thumb = $podcast['thumbnail']['small'];
        $p_iframe = '<iframe id="video_player" src="https://embed.vhx.tv/videos/' . $p_id . '?autoplay=0&api=1" width="700" height="450" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            ?>
            <div class="podcast-player-wrap">
                <?php echo $p_iframe; ?>  
            </div>
            <div class="movie-thumb podcast-thumb">
                <a href="#"><img src="<?php echo $p_thumb; ?>" alt="Podcast" /></a>
            </div>

            <?php 

    //    echo $podcast['id'];

    }    
    
    ?>
    
    </div>
    </div>

</div>
<div class="podcast-footer">
<?php 

get_footer();

?>
</div>