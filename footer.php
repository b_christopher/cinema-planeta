<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Cinema_Planeta
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="two-col-wrap">
            <div class="footer-col col1">
                <ul class="footer-menu">
                    <?php wp_nav_menu( array( 
    'theme_location' => 'footer-menu-1' 
    ) );?>
                    
<!--
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Life Streaming</a></li>
                    <li><a href="#">Podcast</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">My Account</a></li>
-->
                </ul>                
                
                <ul class="footer-menu footer-menu-2">
                    
                                        <?php wp_nav_menu( array( 
    'theme_location' => 'footer-menu-2' 
    ) );?>
<!--
                    <li><a href="#">Support</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Cookies Policy</a></li>
                    <li><a href="#">Contact Us</a></li>
-->
                </ul>
            </div>
            <div class="footer-col col2">
                <p class="copyright">2017 CINEMA PLANETA copyright</p>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
