<?php
/* Template Name: Single Movie Page */ 
 
get_header(); 

//Current Video ID
if(isset($_GET["video_id"])) {
    $current_id = $_GET["video_id"];
}

//Set API Key
\VHX\Api::setKey("GsqqcWX7gEAojnP1ay79HT2DRjLtwR8D");

$current_video = \VHX\Videos::retrieve("https://api.vhx.tv/videos/$current_id");

//echo '<pre>';
//print_r($current_video);
//echo '</pre>';

//print_r($desc);

//Vars
$title = $current_video['title'];
$desc = $current_video['description'];
$dur = $current_video['duration']['formatted'];
$created = $current_video['created_at'];
//$year = substr($created, 0, 4);
$dur_h = substr($dur, 0, 2);
$dur_m = substr($dur, -5, 2);
$img_url = $current_video['thumbnail']['source'];
$current_iframe = '<iframe id="video_player" src="https://embed.vhx.tv/videos/' . $current_id . '?autoplay=0&api=1" width="700" height="450" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

$json = $current_video['description'];
$desc = json_decode($json, true);
$genre = $desc['genre'];
$year = $desc['year'];
$country = $desc['country'];
$desc_text = $desc['description'];
$director = $desc['director'];

//print_r($desc);
//echo $genre;
//echo $year;

?>
<div class="player-wrap">
<?php 
if(is_user_logged_in()){
        echo $current_iframe; 

 } else{
    
    echo '<div class="player-login-msg"><h2>You Must login to view this video.</h2><a href="/#signin">Click here to signup/login.</a></div>';
}
    
    
    
    ?>
</div>
<div class="single-movie">
<div class="single-movie-bg" style="background: linear-gradient(to bottom, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .5) 59%, rgba(0, 0, 0, 0.85) 100%), url('<?php echo $img_url; ?>') no-repeat; background-size: cover;">
    <a href="#"><img src="/wp-content/themes/cinema-planeta/images/single-play-btn.png" class="single-play-btn" alt="Play"/></a>
</div>    
<div class="current-movie-content-wrap">    
<div class="current-movie-content">
        <div class="title-and-ratings">
            <h1 class="movie-title"><?php echo $title; ?></h1>
            <div class="movie-type-wrap">
                <div class="movie-type-box"></div>
                    <h2 class="movie-type">Documental</h2>
                    <div class="movie-ratings"><img src="/wp-content/themes/cinema-planeta/images/empty-rating.png" alt="Rating" class="empty-stars"></div>
            </div>
        </div>
        
        <div class="movie-inner">
            <div class="movie-meta-info">
                <p class="quality">HD</p>
                <p class="year"><?php echo $year; ?></p>
                <p class="length">
                <?php 
                    
                    if ($dur_h == 00){
                        
                    }else{
                       echo $dur_h . 'h '; 
                    }
                    
                    echo $dur_m . 'm';
                     ?>
                </p>
                <p class="category">Historical</p>
            </div>

            <p class="movie-description">
            <?php echo $desc_text; ?>
            </p>
          </div>

        <div class="movie-details">
            <h4>Movie Details</h4>
            <ul class="movie-details-list">
                <li>Genre: <?php echo $genre; ?></li>
                <li>Director: <?php echo $director; ?></li>
                <li>Country: <?php echo $country; ?></li>
                <li>Year: <?php echo $year; ?></li>
            </ul>

            <ul class="movie-cats">
                <li>Documentary</li>
                <li>History</li>
                <li>Fidel Castro</li>
                <li>Revolution</li>
            </ul>
        </div>    

            
</div>
</div>    

<div class="similar-docs">
    <h2>Similar Documentaries</h2>
    <div class="sim-grid movie-grid">
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>        
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>        
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>        
        <div class="movie-thumb">
            <img src="/wp-content/themes/cinema-planeta/images/movie-cover.jpg" alt="Maradona" />
        </div>
    </div>
</div>

</div>
<?php 

get_footer();

?>