<?php
/* Template Name: Main Page */ 

//Set API Key
\VHX\Api::setKey("GsqqcWX7gEAojnP1ay79HT2DRjLtwR8D");

//List all Videos
$videos = \VHX\Videos::all()['_embedded']['videos'];

$products = \VHX\Products::all();

$collections = \VHX\Collections::all();

$collect_items = $collections['_embedded']['collections'];

//echo '<pre>';
//print_r($collect_items);
//echo '</pre>';

//echo $products['_embedded']['products'][0]['movies'];

//Featured Variables
$f_id = $videos[3]['id'];
$f_title = $videos[3]['title'];
$f_desc = $videos[3]['description'];
$f_dur = $videos[3]['duration']['formatted'];
$f_created = $videos[3]['created_at'];
$f_year = substr($f_created, 0, 4);
$f_dur_h = substr($f_dur, 0, 2);
$f_dur_m = substr($f_dur, -5, 2);
$f_img_url = $videos[3]['thumbnail']['source'];
$f_iframe = '<iframe id="video_player" src="https://embed.vhx.tv/videos/' . $f_id . '?autoplay=0&api=1" width="700" height="450" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

$json = $videos[3]['description'];
$desc = json_decode($json, true);
$genre = $desc['genre'];
$year = $desc['year'];
$country = $desc['country'];
$desc_text = $desc['description'];
$director = $desc['director'];

get_header(); 

//Include Signup Form
get_template_part( 'template-parts/content', 'signup' );
?>
<div class="player-wrap">
<?php 
if(is_user_logged_in()){
        echo $f_iframe; 

 } else{
    
    echo '<div class="player-login-msg"><h2>You Must login to view this video.</h2><a href="/#signin">Click here to signup/login.</a></div>';
}
    
    ?>
</div>
<div class="main-page">
<div class="current-movie-wrap" style="background: linear-gradient(to bottom, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .5) 59%, rgba(0, 0, 0, 0.85) 100%), url('<?php echo $f_img_url; ?>') no-repeat">
    <div class="current-movie-content">
        <div class="title-and-ratings">
            <h1 class="movie-title"><?php echo $f_title; ?></h1>
            <div class="movie-type-wrap">
                <div class="movie-type-box"></div>
                    <h2 class="movie-type">Documental</h2>
                    <div class="movie-ratings"><img src="/wp-content/themes/cinema-planeta/images/empty-rating.png" alt="Rating" class="empty-stars"></div>
            </div>
        </div>
        
        <div class="movie-inner">
            <div class="movie-meta-info">
                <p class="quality">HD</p>
                <p class="year"><?php echo $f_year; ?></p>
                <p class="length"><?php 
                    
                    if ($f_dur_h == 00){
                        
                    }else{
                       echo $f_dur_h . 'h '; 
                    }
                    
                    echo $f_dur_m . 'm';
                     ?>
                
                </p>
                <p class="category">Historical</p>
            </div>

            <p class="movie-description">
            <?php echo $f_desc; ?>
            </p>

            
            <div class="movie-inner-btns">
                <a href="#" class="play-movie-btn blue-btn"><img src="/wp-content/themes/cinema-planeta/images/play-btn-triangle.png" class="play-btn" alt="Play" />Play</a>
                <a href="#" class="play-trailer-btn purple-btn"><img src="/wp-content/themes/cinema-planeta/images/trailer-btn.png" class="play-btn" alt="Trailer" />Trailer</a>
            </div>
        </div>
    </div>
</div>
<div class="movie-gallery">
<!--
<div class="continue-watching">
    <h2>Continue Watching</h2>
    <div class="continue-grid">
        <div class="movie-continue">
           <img src="/wp-content/themes/cinema-planeta/images/movie-con-1.jpg" alt="Continue Watching" />
        </div>
        
        <div class="movie-continue">
            <img src="/wp-content/themes/cinema-planeta/images/movie-con-2.jpg" alt="Continue Watching" />
        </div>
    </div>
</div>
-->
    
<div class="recently-added">
    <h2>Recently Added</h2>
    <div class="recent-grid movie-grid">
        <?php 
        
        foreach ($videos as $video){
            
            $video_id = $video['id'];
            $video_title = $video['title'];
            $thumb_url = $video['thumbnail']['small'];
            
//            echo '<pre>';
//            print_r($video);
//            echo '</pre>';
            
            if ($video['metadata']['movie_name'] == 'Podcasts'){
                
            }else{
                
            echo '<div class="movie-thumb">
            <a href="/single-movie?video_id=' . $video_id . '">
            <img src="' . $thumb_url .'" alt="' . $video_title . '" />
            </a>
            </div>';
            }
            

        }
        
        ?>

    </div>
</div>

    
<?php 

//All Collections
    
foreach ($collect_items as $item){
    
    $collect_name = $item['name'];
    $collect_id = $item['id'];
    $collect_movies = \VHX\Collections::items($collect_id)['_embedded']['items'];    
//    echo '<pre>';
//    
////    print_r($collect_movies);
//    
//    echo '</pre>';
    
    
    if ($collect_name == 'Podcasts' || $collect_name == 'Conferencias'){
        
    }else{

        echo '<div class="collection">';

        echo '<h2>' . $collect_name . '</h2>';
        
        echo '<div class="movie-grid">';
        
        foreach ($collect_movies as $movie){
                        
            $movie_thumb = $movie['thumbnail']['small'];
            $movie_id = $movie['id'];
        ?>
    
            <div class="movie-thumb">
                <a href="/single-movie?video_id=<?php echo $movie_id; ?>"><img src="<?php echo $movie_thumb; ?>" alt="Movie" /></a>
            </div>
    
        <?php
        }
        
        echo '</div>';
        
        echo '</div>';
    
    }
}    
    
//Podcasts    
//$p_collections = \VHX\Collections::items(39782);    
//$podcasts = $p_collections['_embedded']['items'];
//echo '<pre>';    
//print_r($podcasts);    
//echo '</pre>';
?>
<!--
    
    <div class="podcasts">
    <h2>Podcasts</h2>
    <div class="recent-grid movie-grid">
-->

    <?php
//        
//    foreach ($podcasts as $podcast){
//        $p_id = $podcast['id'];    
//        $p_thumb = $podcast['thumbnail']['small'];
//        $p_iframe = '<iframe id="video_player" src="https://embed.vhx.tv/videos/' . $p_id . '?autoplay=0&api=1" width="700" height="450" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
            ?>
<!--
            <div class="podcast-player-wrap">
                <?php //echo $p_iframe; ?>  
            </div>
            <div class="movie-thumb podcast-thumb">
                <a href="#"><img src="<?php //echo $p_thumb; ?>" alt="Podcast" /></a>
            </div>
-->

            <?php 

    //    echo $podcast['id'];

   // }    
    
    ?>
    
   <!--  </div>
    </div> -->

</div>
<?php 

get_footer();

?>