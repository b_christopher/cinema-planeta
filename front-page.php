<?php 
/* Template Name: Landing Page */ 

//Load Scripts and Styles
wp_head();

//Check if user is logged in
if (is_user_logged_in() ) {
        wp_redirect( "/main" ); 
        exit();
}

//Include Signup Form
get_template_part( 'template-parts/content', 'signup' );

?>

<div class="landing-intro">

    <div class="landing-header two-col-wrap">
        <div class="header-col">
            <a href="/" class="logo-link"><img class="logo" src="/wp-content/themes/cinema-planeta/images/logo.png" alt="Cinema Planeta"/></a>
        </div>
        <div class="header-col landing-signin open-btn">
            <a class="signin-btn blue-btn">Sign In</a>
        </div>
    </div>
    
    <div class="intro-content">
        <h1>See whats Next!</h1>
        <h3>Watch anywhere, cancel anytime</h3>
        <a class="signup-btn blue-btn open-btn">Sign Up</a>
    </div>    
    
</div>

<div class="three-col-wrap button-grid">
    <div class="watch-any">
        <img class="grid-icon" src="/wp-content/themes/cinema-planeta/images/watch-anywhere-icon.png" alt="Watch Anywhere" />
        <h4>Watch Anywhere</h4>
        <div class="active-grid-btn"></div>
    </div>
    <div class="ext-lib">
        <img class="grid-icon" src="/wp-content/themes/cinema-planeta/images/extensive-library-icon.png" alt="Watch Anywhere" />
        <h4>Extensive Library</h4>
    </div>
    <div class="ex-content">
        <img class="grid-icon" src="/wp-content/themes/cinema-planeta/images/exclusive-content-icon.png" alt="Watch Anywhere" />
        <h4>Exclusive Content</h4>
    </div>
</div>

<div class="button-grid-content">
    <div class="two-col-wrap">
        <div class="grid-content-col col1">
            <img src="/wp-content/themes/cinema-planeta/images/any-phone.png" alt="Watch Anywhere" />
            <h4>Watch Anywhere</h4>
            <p>Available on phone and tablet<br> wherever you go</p>
        </div>
        <div class="grid-content-col col2">
            <img src="/wp-content/themes/cinema-planeta/images/any-computer.png" alt="Watch Anywhere" />
            <h4>Use any Computer</h4>
            <p>Your favorite videos<br> available wherever you want</p>
        </div>
    </div>
</div>

<?php



			while ( have_posts() ) : the_post();

			endwhile; // End of the loop.


get_footer();

?>