'use strict';

var gulp = require( 'gulp' );
var browserSync = require( 'browser-sync' ).create();
var sass = require( 'gulp-ruby-sass' );

gulp.task( 'sass', function() {
    return sass( 'sass/style.scss' )
        .on( 'error', sass.logError )
        .pipe( gulp.dest( './' ) )
        .pipe( browserSync.stream() );
} );


gulp.task( 'watch', function() {
    browserSync.init( {
        files: [ './**/*.php', '*.php' ],
        proxy: 'http://cp.test',
    } );
    gulp.watch( [ './sass/**/*.scss' ], [ 'sass' ] );
} );