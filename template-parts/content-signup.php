<!--- Signup/Register Overlay -->
<div class="signup-overlay" id="overlay">
    <div class="signup-wrap" id="wrap">
    <div class="signup-overlay-x">X</div>


        <div class="login-form-wrap">
            
            <?php
            
            //Check for login failure
            $login 	= (isset($_GET['login']) ) ? $_GET['login'] : 0;
            $err_msg = '';
            
            
            if ( $login === "failed" ) {
                $err_msg = '<p class="login-msg"><strong>ERROR:</strong> Invalid username and/or password.</p>';
            } elseif ( $login === "empty" ) {
                $err_msg = '<p class="login-msg"><strong>ERROR:</strong> Username and/or Password is empty.</p>';
            } elseif ( $login === "false" ) {
                $err_msg = '<p class="login-msg"><strong>ERROR:</strong> You are logged out.</p>';
            }
            
            
            $args = array(
                'redirect' => '/main', 
                'id_username' => 'user',
                'id_password' => 'pass',
               ) 
            ;

            wp_login_form( $args ); 
            
            //Show error msg
            echo $err_msg;
            

    
             ?>

        </div>

        <div class="register-form-wrap">
            <form class="register-form" name="registration">
                <h2>Register</h2>
                <div class="two-col-wrap">
                    <div class="col col1">
                        <input type="text" name="register-user" id="register-user" placeholder="Username" required>    
                        <input type="text" name="register-first" id="register-first" placeholder="First Name" required>    

                        <input type="text" name="register-last" id="register-last" placeholder="Last Name" required> 
                    </div>
                    <div class="col col2">
                        <input type="email" name="register-email" id="register-email" placeholder="Email" required>

                        <input type="password" name="registerpass" id="registerpass" placeholder="Password" required> 

                        <input type="password" name="registerpassconfirm" id="registerpassconfirm" placeholder="Password (repeat)" required> 
                    </div>
                </div>

                <div class="social-signup-wrap">
                    <div class="social-signup"><span class="signup-text">Or sign up using:</span> 
                        <img src="/wp-content/themes/cinema-planeta/images/FBIcon.png" alt="Facebook Signup" class="facebook-signup" /> 
                        <img src="/wp-content/themes/cinema-planeta/images/GoogleIcon.png" alt="Google Signup" class="google-signup" />
                        </div>
                </div>

                <input type="submit" class="signup-submit" value="Start Membership!">

                <a href="/main" class="not-now">Later, continue</a>

                <p class="agree-text">By Registering you agree to our Terms, Cookies Policy, & Privacy Policy.</p>

            </form>
            <a href="#" class="need-help-btn">Need Help?</a>

        </div>

    </div>
</div>


<?php 

//PayPal Integration

//PayPal URL or sandbox URL.
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
//seller email id.
$merchant_email = 'brady.s.christopher@gmail.com';
//URL when payment is not completed.
$cancel_return = "http://cp.test/#signin-paypal";
//URL when payment is Successful.
$success_return = "http://cp.test/main#paypal";
?>

<!-- Plan Options Overlay -->
<div class="options-overlay" id="plan-options-overlay">
    <div class="options-wrap">
        <div class="options-overlay-x">X</div>
        
        <div class="plan-row">
            <h2>Choose your Plan!</h2>
            <h3>Cancel online at anytime.</h3>
            
            <div class="two-col-wrap">
            <div class="full-access">
                <h3>Full Access!</h3>
                <p>This plan includes:</p>
                <ul>
                    <li>VOD</li>
                    <li>Live</li>
                    <li>Podcasts</li>
                </ul>
                <a href="#" class="sub-btn full-btn">Subscribe!</a>
            </div>
            
            <div class="pay-as-you-go">
                <h3>Pay as you go!</h3>
                <p>This plan allows the user to browse the gallery without having to pay a monthly fee but the non-free content will have to be unlocked by buying it.</p>
                <a href="/main" class="sub-btn pay-btn">Subscribe!</a>
            </div>
            </div>
        </div>
        
        <div class="paypal-setup-row" style="display: none;">
            <h2>Set up your Payment</h2>
            <h3>No commitments, Cancel anytime.</h3>
            <div class="paypal-setup">
                <h3>PayPal</h3>
                <p>PayPal allows users to pay through a PayPal account or by credit card, all of this using PayPal's own flow.</p>
                
                <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    
                <input type ="hidden" name ="business" value ="<?php echo $merchant_email; ?>">    
                   
                <input type ="hidden" name ="cancel_return" value ="<?php echo $cancel_return ?>">
                    
                <input type ="hidden" name ="return" value ="<?php echo $success_return; ?>">     
                    
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="LJHT4K88VQHJ6">
                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" class="paypal-pay">
                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>

                
            </div>
            
        </div>
        
        <div class="paypal-success-row" style="display: none;">
<?php    
                
            if ($_REQUEST['auth'] != '') { ?>
                    <h2>Payment Successful!</h2>
                    <a class="success-link" href="#">Click here to continue.</a>
            <?php } else {?>
                    <h2>Payment Failed.</h2>
                    <p>Please try again.</p>
            <?php }

?>
            
            
            
            

        </div>
        
        <div class="success-row" style="display: none;">
            <h2>Registration Successful!</h2>
            <a href="/main">Click here to continue.</a>
        </div>
        
    </div>
</div>