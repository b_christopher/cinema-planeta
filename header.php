<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Cinema_Planeta
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
    
<?php
//    
//    $user = wp_get_current_user();
//    var_dump($user);
?>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'cinema-planeta-theme' ); ?></a>

	<header id="masthead" class="site-header">
		
        <div class="three-col-wrap">
            <div class="header-menu">
                
                <button class="menu-toggle-btn" aria-controls="primary-menu" aria-expanded="false">
                <div class="two-col-wrap">
                <div class="menu-btn-text">
                <?php esc_html_e( 'Menu', 'cinema-planeta-theme' ); ?> 
                </div>

                <div class="bar-container">
                  <div class="bar1"></div>
                  <div class="bar2"></div>
                  <div class="bar3"></div>
                </div>
                </div>
                </button>
                
                <nav id="site-navigation" class="main-navigation">
                    <ul id="main-nav">
                        
                        <?php wp_nav_menu( array( 
    'theme_location' => 'main-menu' 
    ) ); ?>
<!--
                        
                        <li class="active">Home</li>
                        <li>Life Streaming</li>
                        <li>Podcast</li>
                        <li>Blog</li>
                        <li>My Account</li>
-->
                    </ul>
                </nav><!-- #site-navigation -->
                
            </div>
            <div class="header-logo">
                <a href="/" class="logo-link"><img class="logo" src="/wp-content/themes/cinema-planeta/images/logo-small.png" alt="Cinema Planeta"/></a>
            </div>
            <div class="header-search">
                <img src="/wp-content/themes/cinema-planeta/images/search-icon.png" class="search-icon" alt="Image Search" />
                <input type="search" placeholder="Buscar">
            </div>
        
        </div>
        
        

        
        
	</header><!-- #masthead -->

	<div id="content" class="site-content">
